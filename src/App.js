import { Routes, Route } from "react-router-dom";
import './App.css';
import HomePage from './components/home/home';
import AppHeader from './components/common/header';
import CreaetClient from "./components/clients/CreaetClient";
import EditClient from "./components/clients/EditClient";
import Worksites from "./components/worksites/Worksites";
import Employees from "./components/employees/employees";
import Equipment from "./components/equipment/equipment";
import Jobs from "./components/jobs/jobs";
import ClientsList from "./components/clients/clientsList";
import CreaetWorksite from "./components/worksites/CreaeteWorksite";
import EditWorksite from "./components/worksites/EditWorksite";
import CreaeteEmployee from "./components/employees/CreaeteEmployee";
import EditEmployee from "./components/employees/EditEmployee";
import CreaeteEquipment from "./components/equipment/CreaeteEquipment";
import EditEquipment from "./components/equipment/EditEquipment";
import CreaetJob from "./components/jobs/CreaeteJob";
import EditJob from "./components/jobs/EditJob";

function App() {
  return (
    <div className="App">
      <AppHeader/>
      <Routes>
        <Route path="/" element={<HomePage />}/>
        <Route path="/clients/list" element={<ClientsList />}/>
        <Route path="/clients/create" element={<CreaetClient />}/>
        <Route path="/clients/edit/:id" element={<EditClient />}/>
        <Route path="/worksites/list" element={<Worksites />}/>
        <Route path="/worksites/create" element={<CreaetWorksite />}/>
        <Route path="/worksites/edit/:id" element={<EditWorksite />}/>
        <Route path="/employees/list" element={<Employees />}/>
        <Route path="/employees/create" element={<CreaeteEmployee />}/>
        <Route path="/employees/edit/:id" element={<EditEmployee />}/>
        <Route path="/equipment/list" element={<Equipment />}/>
        <Route path="/equipment/create" element={<CreaeteEquipment />}/>
        <Route path="/equipment/edit/:id" element={<EditEquipment />}/>

        <Route path="/jobs/list" element={<Jobs />}/>
        <Route path="/jobs/create" element={<CreaetJob />}/>
        <Route path="/jobs/edit/:id" element={<EditJob />}/>


      </Routes>
    </div>
  );
}

export default App;
