import React from 'react'

const Select = props => {

  return (
    <label>
    {props.children}
      <select value={props.defaultValue} onChange={props.onChange}>
          {props.optionsList.map((option, index) => (
              <option key={index} value={option.id || option.name}>{option.name}</option>
          ))}
      </select>
</label>
  )
}

export default Select