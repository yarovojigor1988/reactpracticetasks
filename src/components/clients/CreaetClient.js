import React, { useState } from 'react';
import styles from './CreaetClient.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';

export default function CreaetClient() {
    let [type, setType] = useState("Company");
    let [name, setName] = useState("");
    let [officeAdress, setOfficeAdress] = useState("");
    let [contactPhone, setContactPhone] = useState("");
    let [contactPerson, setContactPerson] = useState("");
    let [status, setStatus] = useState("True");

    const sendUser = async event => {
        event.preventDefault();
        
        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (officeAdress.trim() === '' || officeAdress.trim().length < 2 ) {
            alert('Office Adress must be longer than 1 character');
            return
        } else if (contactPhone.length < 5 ) {
            alert('Contact Phone must be longer than 5 numbers');
            return
        } else if (contactPerson.trim() === '' || contactPerson.trim().length < 2 ) {
            alert('Contact Person must be longer than 1 character');
            return
        } 
       
        event.target.disabled = true;

        const user = {
            name,
            officeAdress,
            contactPhone,
            contactPerson,
            type,
            status,
        }
        try {
            await axios.post('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client.json',user);
            setType('Company');
            setName('');
            setOfficeAdress('');
            setContactPhone('');
            setContactPerson('');
            setStatus('True');
            event.target.disabled = false;

        } catch(e) {
            console.log(e);
        }

    }

    return (
        <div>
            <h2>Clients</h2>

            <form className={styles.ClientsForm}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                    Primary office address:
                    <input type="text" name="officeAddress" value={officeAdress} onChange={e => setOfficeAdress(e.target.value)} />
                </label>
                <label>
                    Contact Phone:
                    <input type="number" name="contactPhome" value={contactPhone} onChange={e => setContactPhone(e.target.value)} />
                </label>
                <label>
                    Contact person:
                    <input type="text" name="contactPerson" value={contactPerson} onChange={e => setContactPerson(e.target.value)} />
                </label>

                <label>
                    Type:
                    <select value={type} onChange={e => setType(e.target.value)}>
                        <option value="company">Company</option>
                        <option value="individual">Individual</option>
                    </select>
                </label>

                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='true'>True</option>
                        <option value='false'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={sendUser}
                >
                    Send user data
                </Button>
            </form>
        </div>
    );
}