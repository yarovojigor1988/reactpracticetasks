import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import styles from './CreaetClient.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';

export default function EditClients() {
    let [type, setType] = useState("Company");
    let [name, setName] = useState("");
    let [officeAdress, setOfficeAdress] = useState("");
    let [contactPhone, setContactPhone] = useState("");
    let [contactPerson, setContactPerson] = useState("");
    let [status, setStatus] = useState("True");

    let { id } = useParams();
    useEffect(() => {        
        axios.get(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client/${id}.json`).then(res => {
            setType(res.data.type);
            setName(res.data.name);
            setOfficeAdress(res.data.officeAdress);
            setContactPhone(res.data.contactPhone);
            setContactPerson(res.data.contactPerson);
            setStatus(res.data.status);
        })
    },[id]);

    let navigate = useNavigate();

    const editUser = async event => {
        event.preventDefault();
                
        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (officeAdress.trim() === '' || officeAdress.trim().length < 2 ) {
            alert('Office Adress must be longer than 1 character');
            return
        } else if (contactPhone.length < 5 ) {
            alert('Contact Phone must be longer than 5 numbers');
            return
        } else if (contactPerson.trim() === '' || contactPerson.trim().length < 2 ) {
            alert('Contact Person must be longer than 1 character');
            return
        }        

        const user = {
            name,
            officeAdress,
            contactPhone,
            contactPerson,
            type,
            status,
        }
        try {
            await axios.put(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client/${id}.json`,user);
            navigate('/clients/list')
        } catch(e) {
            console.log(e);
        }

    }

    return (
        <div>
            <h2>Clients</h2>

            <form className={styles.ClientsForm}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                    Primary office address:
                    <input type="text" name="officeAddress" value={officeAdress} onChange={e => setOfficeAdress(e.target.value)} />
                </label>
                <label>
                    Contact Phone:
                    <input type="number" name="contactPhome" value={contactPhone} onChange={e => setContactPhone(e.target.value)} />
                </label>
                <label>
                    Contact person:
                    <input type="text" name="contactPerson" value={contactPerson} onChange={e => setContactPerson(e.target.value)} />
                </label>

                <label>
                    Type:
                    <select value={type} onChange={e => setType(e.target.value)}>
                        <option value="Company">Company</option>
                        <option value="Individual">Individual</option>
                    </select>
                </label>

                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='True'>True</option>
                        <option value='False'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={editUser}
                >
                    Send user data
                </Button>
            </form>
        </div>
    );
}