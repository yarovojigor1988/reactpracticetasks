import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './clientsList.module.scss';
import Button from '../../UI/Button/Button';
import { useNavigate } from 'react-router-dom';

export default function ClientsList() {
    let [users, setUsers] = useState([]);
    let navigate = useNavigate();

    const getUsers = async function () {
        const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client.json');
        let usersData = []
        if (res.data !== null) {
            for (const [key, value] of Object.entries(res.data)) {
                let user = value;
                user.id = key;
                usersData.push(user)
            }            
        }
        setUsers(usersData);
    }

    const deleteUser = async (id) => {
        const resWorksites = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites.json');
        let worksitesData = []
        if (resWorksites.data !== null) {
            for (const [key, value] of Object.entries(resWorksites.data)) {
                let Worksite = value;
                Worksite.id = key;
                worksitesData.push(value)
            }            
        }
        let optionClient = worksitesData.find(el => el.client === id);
        // if (typeof(optionClient) !== 'undefined' && optionClient.status === 'True') {
            if (typeof(optionClient) !== 'undefined') {
            alert('Client have active worksite');
            getUsers();
            return;

        }
        await axios.delete(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client/${id}.json`);
        getUsers();
    }

    const editUser = (id) => {
        navigate(`/clients/edit/${id}`)
    }


    const CreateNewUser = () => {
        navigate('/clients/create')
    }

    useEffect(() => {
        getUsers();
    }, []);

    return (
        <div>
            <h2>Clients List</h2>

            <div className={styles.usersTable}>
                <div className={styles.usersRow}>
                    <div className={styles.usersCell}>Name</div>
                    <div className={styles.usersCell}>Office Adress</div>
                    <div className={styles.usersCell}>Contact Phone</div>
                    <div className={styles.usersCell}>Contact Person</div>
                    <div className={styles.usersCell}>Type</div>
                    <div className={styles.usersCell}>Status</div>
                    <div className={styles.usersCell}></div>
                </div>
                {users.map((user, index) => (
                    <div className={styles.usersRow} key={index}>
                        <div className={styles.usersCell}>{user.name}</div>
                        <div className={styles.usersCell}>{user.officeAdress}</div>
                        <div className={styles.usersCell}>{user.contactPhone}</div>
                        <div className={styles.usersCell}>{user.contactPerson}</div>
                        <div className={styles.usersCell}>{user.type}</div>
                        <div className={styles.usersCell}>{user.status}</div>
                        <div className={styles.usersCell}>
                            <Button
                                type="primary"
                                onClick={() => { editUser(user.id) }}
                            >
                                Edit
                            </Button>
                            <Button
                                type="error"
                                onClick={() => { deleteUser(user.id) }}
                            >
                                Delete
                            </Button>
                        </div>
                    </div>
                ))}
            </div>

            <Button
                type="primary"
                onClick={CreateNewUser}
            >
                Create new Client
            </Button>
        </div>
    )
}
