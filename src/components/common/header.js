import { Link } from "react-router-dom";
import classes from './header.module.scss'

function AppHeader() {
    return (
        <div className={classes.header}>
            <h1>Cleaner Track Pro</h1>
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/clients/list">ClientsList</Link>
                    </li>
                    <li>
                        <Link to="/employees/list">Employees</Link>
                    </li>
                    <li>
                        <Link to="/equipment/list">Equipment</Link>
                    </li>
                    <li>
                        <Link to="/jobs/list">Jobs</Link>
                    </li>
                    <li>
                        <Link to="/worksites/list">Worksites</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default AppHeader;
