import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import styles from './CreaeteEmployee.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';

export default function EditEmployee() {
    let [name, setName] = useState("");
    let [Address, setAddress] = useState("");
    let [Phone, setPhone] = useState("");
    let [Salary, setSalary] = useState("");
    let [DateOfBirth, setDateOfBirth] = useState(new Date());
    let [status, setStatus] = useState("True");

    let { id } = useParams();

    useEffect(() => {        
        axios.get(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/employees/${id}.json`).then(res => {
            setName(res.data.name);
            setAddress(res.data.Address);
            setPhone(res.data.Phone);
            setSalary(res.data.Salary);
            setStatus(res.data.status);
            setDateOfBirth(new Date(res.data.DateOfBirth));
        })
    },[id]);

    let navigate = useNavigate();

    const editUser = async event => {
        event.preventDefault();
                        
        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (Address.trim() === '' || Address.trim().length < 2 ) {
            alert('Adress must be longer than 1 character');
            return
        } else if (Salary <= 0 ) {
            alert('Salary must must be greater than 0 ');
            return

        } else if (Phone.length < 5 ) {
            alert('Phone must be longer than 5 numbers');
            return
        } 
       
        const user = {
            name,
            Address,
            Phone,
            Salary,
            DateOfBirth : DateOfBirth.toLocaleDateString(),
            status,
        }
        try {
            await axios.put(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/employees/${id}.json`,user);
            navigate('/employees/list')
        } catch(e) {
            console.log(e);
        }

    }

    return (
        <div>
            <h2>Create new employee</h2>

            <form className={styles.Form}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                    Address:
                    <input type="text" name="Address" value={Address} onChange={e => setAddress(e.target.value)} />
                </label>
                <label>
                    Phone:
                    <input type="number" name="Phome" value={Phone} onChange={e => setPhone(e.target.value)} />
                </label>
                <label>
                Salary:
                    <input type="number" name="Salary" value={Salary} onChange={e => setSalary(e.target.value)} />
                </label>
                <label>
                    <span>Date of birth:</span>
                    <DatePicker 
                        selected={DateOfBirth}
                        showYearDropdown
                        showMonthDropdown
                        dropdownMode="select"
                        onChange={(date) => setDateOfBirth(date)} />
                </label>
                

                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='true'>True</option>
                        <option value='false'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={editUser}
                >
                    Send user data
                </Button>
            </form>
        </div>
    );
}