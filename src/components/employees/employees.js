import React, { useState, useEffect } from 'react';
import styles from './Employees.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';
import { useNavigate } from 'react-router-dom';


function Employees() {
  // let [clientsList, setClientsList] = useState({});
  let [Employees, setEmployees] = useState([]);
  let navigate = useNavigate();
  let [jobs, setJobs] = useState([]);

  const getJobs = async function () {
      const jobsRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs.json');
      let jobsData = []
      if (jobsRes.data !== null) {
          for (const [key, value] of Object.entries(jobsRes.data)) {
              let job = value;
              job.id = key;
              jobsData.push(job)
          }            
      }
      setJobs(jobsData);
  }

  const getEmployees = async function () {

      const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/employees.json');
      let EmployeesData = []
      if (res.data !== null) {
          for (const [key, value] of Object.entries(res.data)) {
              let employee = value;
              employee.id = key;
              EmployeesData.push(employee)
          }            
      }
      setEmployees(EmployeesData);
  }

  const deleteEmployee = async (id) => {

    let optionEmployee = jobs.find(el => el.Employee === id);
    if (typeof(optionEmployee) !== 'undefined') {
        alert('Employee have active job');
        getEmployees();
        return;
    }

      await axios.delete(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/employees/${id}.json`);
      getEmployees();

  }

  const editEmployee = (id) => {
      navigate(`/Employees/edit/${id}`)
  }


  const CreateNewEmployee = () => {
      navigate('/Employees/create')
  }

  useEffect(() => {
      getEmployees();
      getJobs();
  }, []);
  return (
      <div>
          <h2>Employees List</h2>

          <div className={styles.Table}>
              <div className={styles.Row}>
                  <div className={styles.Cell}>Name</div>
                  <div className={styles.Cell}>Adress</div>
                  <div className={styles.Cell}>Phone</div>
                  <div className={styles.Cell}>Salary</div>
                  <div className={styles.Cell}>Date of birth</div>                  
                  <div className={styles.Cell}>Status</div>
                  <div className={styles.Cell}></div>
              </div>
              {Employees.map((employee, index) => {
                  return (
                  <div className={styles.Row} key={index}>
                      <div className={styles.Cell}>{employee.name}</div>
                      <div className={styles.Cell}>{employee.Address}</div>
                      <div className={styles.Cell}>{employee.Phone}</div>
                      <div className={styles.Cell}>{employee.Salary}</div>
                      <div className={styles.Cell}>{employee.DateOfBirth}</div>
                      <div className={styles.Cell}>{employee.status}</div>
                      <div className={styles.Cell}>
                          <Button
                              type="primary"
                              onClick={() => { editEmployee(employee.id) }}
                          >
                              Edit
                          </Button>
                          <Button
                              type="error"
                              onClick={() => { deleteEmployee(employee.id) }}
                          >
                              Delete
                          </Button>
                      </div>
                  </div>
              )})}
          </div>

          <Button
              type="primary"
              onClick={CreateNewEmployee}
          >
              Create new Employee
          </Button>
      </div>
  )
}

export default Employees;