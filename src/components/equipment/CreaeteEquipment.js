import React, { useState } from 'react';
import styles from './CreaeteEquipment.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';

export default function CreaeteEquipment() {
    let [name, setName] = useState("");
    let [StorageLocation, setStorageLocation] = useState("");
    let [UsageFee, setUsageFee] = useState("");
    let [status, setStatus] = useState("True");

    const sendEquipment = async event => {
        event.preventDefault();
                        
        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (StorageLocation.trim() === '' || StorageLocation.trim().length < 2 ) {
            alert('Storage Location must be longer than 1 character');
            return
        } else if (UsageFee <= 0 ) {
            alert('Usage Fee must must be greater than 0 ');
            return
        } 
       
        event.target.disabled = true;
        const user = {
            name,
            StorageLocation,
            UsageFee,
            status,
        }
        try {
            await axios.post('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments.json',user);
            setName('');
            setStorageLocation('');
            setUsageFee('');
            setStatus('True');            
            event.target.disabled = false;

        } catch(e) {
            console.log(e);
        }

    }

    return (
        <div>
            <h2>Create new Equipment</h2>

            <form className={styles.Form}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                Storage Location:
                    <input type="text" name="StorageLocation" value={StorageLocation} onChange={e => setStorageLocation(e.target.value)} />
                </label>
                <label>
                Usage Fee:
                    <input type="number" name="UsageFee" value={UsageFee} onChange={e => setUsageFee(e.target.value)} />
                </label>
                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='true'>True</option>
                        <option value='false'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={sendEquipment}
                >
                    Send Equipment data
                </Button>
            </form>
        </div>
    );
}