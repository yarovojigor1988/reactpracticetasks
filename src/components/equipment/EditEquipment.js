import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import styles from './CreaeteEquipment.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';

export default function EditEquipment() {
    let [name, setName] = useState("");
    let [StorageLocation, setStorageLocation] = useState("");
    let [UsageFee, setUsageFee] = useState("");
    let [status, setStatus] = useState("True");

    let { id } = useParams();

    useEffect(() => {        
        axios.get(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments/${id}.json`).then(res => {
            setName(res.data.name);
            setStorageLocation(res.data.StorageLocation);
            setUsageFee(res.data.UsageFee);
            setStatus(res.data.status);
        })
    },[id]);

    let navigate = useNavigate();

    const editEquipment = async event => {
        event.preventDefault();
                                
        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (StorageLocation.trim() === '' || StorageLocation.trim().length < 2 ) {
            alert('Storage Location must be longer than 1 character');
            return
        } else if (UsageFee <= 0 ) {
            alert('Usage Fee must must be greater than 0 ');
            return
        }                

        const Equipment = {
            name,
            StorageLocation,
            UsageFee,
            status,
        }
        try {
            await axios.put(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments/${id}.json`,Equipment);
            navigate('/equipment/list')
        } catch(e) {
            console.log(e);
        }

    }
    return (
        <div>
            <h2>Create new Equipment</h2>

            <form className={styles.Form}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                Storage Location:
                    <input type="text" name="StorageLocation" value={StorageLocation} onChange={e => setStorageLocation(e.target.value)} />
                </label>
                <label>
                Usage Fee:
                    <input type="number" name="UsageFee" value={UsageFee} onChange={e => setUsageFee(e.target.value)} />
                </label>
                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='true'>True</option>
                        <option value='false'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={editEquipment}
                >
                    Send Equipment data
                </Button>
            </form>
        </div>
    );
}