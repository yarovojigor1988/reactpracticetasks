import React, { useState, useEffect } from 'react';
import styles from './Equipment.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';
import { useNavigate } from 'react-router-dom';


function Equipment() {
 let [Equipment, setEquipment] = useState([]);
 let navigate = useNavigate();
 let [jobs, setJobs] = useState([]);

 const getJobs = async function () {
     const jobsRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs.json');
     let jobsData = []
     if (jobsRes.data !== null) {
         for (const [key, value] of Object.entries(jobsRes.data)) {
             let job = value;
             job.id = key;
             jobsData.push(job)
         }            
     }
     setJobs(jobsData);
 }

 const getEquipment = async function () {

     const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments.json');
     let EquipmentData = []
     if (res.data !== null) {
         for (const [key, value] of Object.entries(res.data)) {
             let Equipment = value;
             Equipment.id = key;
             EquipmentData.push(Equipment)
         }
     }
     setEquipment(EquipmentData);
 }

 const deleteEquipment = async (id) => {

    let optionEquipment = jobs.find(el => el.Equipment === id);
    if (typeof(optionEquipment) !== 'undefined') {
        alert('Equipment have active job');
        getEquipment();
        return;
    }

    await axios.delete(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments/${id}.json`);
     getEquipment();
 }

 const editEquipment = (id) => {
     navigate(`/Equipment/edit/${id}`)
 }


 const CreateNewEquipment = () => {
     navigate('/Equipment/create')
 }

 useEffect(() => {
     getEquipment();
     getJobs();
 }, []);
 return (
     <div>
         <h2>Equipment List</h2>

         <div className={styles.Table}>
             <div className={styles.Row}>
                 <div className={styles.Cell}>Name</div>
                 <div className={styles.Cell}>Storage Location</div>
                 <div className={styles.Cell}>Usage Fee</div>
                 <div className={styles.Cell}>Status</div>
                 <div className={styles.Cell}></div>
             </div>
             {Equipment.map((Equipment, index) => {
                 return (
                 <div className={styles.Row} key={index}>
                     <div className={styles.Cell}>{Equipment.name}</div>
                     <div className={styles.Cell}>{Equipment.StorageLocation}</div>
                     <div className={styles.Cell}>{Equipment.UsageFee}</div>
                     <div className={styles.Cell}>{Equipment.status}</div>
                     <div className={styles.Cell}>
                         <Button
                             type="primary"
                             onClick={() => { editEquipment(Equipment.id) }}
                         >
                             Edit
                         </Button>
                         <Button
                             type="error"
                             onClick={() => { deleteEquipment(Equipment.id) }}
                         >
                             Delete
                         </Button>
                     </div>
                 </div>
             )})}
         </div>

         <Button
             type="primary"
             onClick={CreateNewEquipment}
         >
             Create new Equipment
         </Button>
     </div>
 )
}

export default Equipment;