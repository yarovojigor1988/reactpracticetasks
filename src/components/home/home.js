import { Link } from "react-router-dom";

function HomePage() {
  return (
    <div className="home-page">
      <h2>Home page</h2>

      <div className="home-navigation">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/clients/list">ClientsList</Link>
          </li>
          <li>
            <Link to="/employees/list">Employees</Link>
          </li>
          <li>
            <Link to="/equipment/list">Equipment</Link>
          </li>
          <li>
            <Link to="/jobs/list">Jobs</Link>
          </li>
          <li>
            <Link to="/worksites/list">Worksites</Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default HomePage;