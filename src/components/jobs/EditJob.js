import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import styles from './CreateJob.module.scss';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Button from '../../UI/Button/Button';
import Select from '../../UI/Select/Select';

export default function EditJob() {
    let [type, setType] = useState('Office cseaning');
    let [Worksite, setWorksite] = useState('');
    let [WorksitesList, setWorksitesList] = useState([]);
    let [HazardousMaterials, setHazardousMaterials] = useState('True');
    let [Employee, setEmployee] = useState('');
    let [EmployeesList, setEmployeesList] = useState([]);
    let [ServiceFee, setServiceFee] = useState('');
    let [Equipment, setEquipment] = useState('');
    let [EquipmentsList, setEquipmentsList] = useState([]);
    let [StartDate, setStartDate] = useState(new Date());
    let [EndDate, setEndDate] = useState(new Date());
    let { id } = useParams();

    const setData = (data, callback) => {
        let tempAraay = [];
        if (data.data !== null) {
            for (const [key, value] of Object.entries(data.data)) {
                let tempObj = value;
                tempObj.id = key;
                tempAraay.push(tempObj)
            }            
        }
        
        if (tempAraay.length > 0 ){
            callback(tempAraay);
        }
    }

    const getJobsDependencies = async function () {
        const WorksitesRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites.json');
        const EmployeesRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/employees.json');
        const EquipmentsRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments.json');

        setData(WorksitesRes, setWorksitesList);
        setData(EmployeesRes, setEmployeesList);
        setData(EquipmentsRes, setEquipmentsList);
    }


    useEffect(() => {   
        async function getJobsData() {
            await getJobsDependencies();
        }
        getJobsData();
        axios.get(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs/${id}.json`).then(res => {
            setType(res.data.type);
            setHazardousMaterials(res.data.HazardousMaterials);
            setServiceFee(res.data.ServiceFee);
            setStartDate(new Date(res.data.StartDate));
            setEndDate(new Date(res.data.EndDate));
            setWorksite(res.data.Worksite)
            setEmployee(res.data.Employee)
            setEquipment(res.data.Equipment)
        })
    },[id]);



    let navigate = useNavigate();

    const sendJob = async event => {
        event.preventDefault();

        if (ServiceFee <= 0 ) {
            alert('Service Fee must must be greater than 0');
            return
        } else if (new Date(StartDate) <= new Date(EndDate)) {
            alert('The work end date must be later than the work start date.');
            return
        }

        const Job = {
            Worksite,
            type,
            HazardousMaterials,
            Employee,
            ServiceFee,
            Equipment,
            StartDate: StartDate.toLocaleDateString(),
            EndDate: EndDate.toLocaleDateString(),
        }

        try {
            await axios.put(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs/${id}.json`,Job);
            navigate('/jobs/list')

        } catch(e) {
            console.log(e);
        }

    }


    const typeList = [
        {
            name: 'Office cleaning'
        },
        {
            name: 'Home cleaning'
        },
        {
            name: 'Deep cleaning'
        },
        {
            name: 'Industrial area cleaning'
        },
        {
            name: 'Outdoor cleaning'
        }
    ]


    return (
        <div>
            <h2>Edit Job</h2>

            <form className={styles.Form}>

                <Select
                        defaultValue={Worksite}
                        onChange={(e)=>{setWorksite(e.target.value)}}
                        optionsList={WorksitesList}
                        >                    
                        Worksite
                </Select>

                <Select
                        defaultValue={type}
                        onChange={(e)=>{setType(e.target.value)}}
                        optionsList={typeList}
                        >                    
                        Type
                </Select>

                <Select
                        defaultValue={HazardousMaterials}
                        onChange={(e)=>{setHazardousMaterials(e.target.value)}}
                        optionsList={[{name: 'True'}, {name: 'False'}]}
                        >                    
                        Hazardous Materials
                </Select>

                <Select
                        defaultValue={Employee}
                        onChange={(e)=>{setEmployee(e.target.value)}}
                        optionsList={EmployeesList}
                        >                    
                        Employee
                </Select>

                <label>
                    Service fee:
                    <input type='number' name='serviceFee' value={ServiceFee} onChange={e => setServiceFee(e.target.value)} />
                </label>

                <Select
                        defaultValue={Equipment}
                        onChange={(e)=>{setEquipment(e.target.value)}}
                        optionsList={EquipmentsList}
                        >                    
                        Additional equipment
                </Select>

                <label>
                    <span>Start date:</span>
                    <DatePicker 
                        selected={StartDate}
                        showYearDropdown
                        showMonthDropdown
                        dropdownMode="select"
                        onChange={(date) => setStartDate(date)} />
                </label>

                <label>
                    <span>End date:</span>
                    <DatePicker 
                        selected={EndDate}
                        showYearDropdown
                        showMonthDropdown
                        dropdownMode="select"
                        onChange={(date) => setEndDate(date)} />
                </label>

                <Button
                    type='success'
                    onClick={sendJob}
                >
                    Send job data
                </Button>
            </form>
        </div>
    );
}