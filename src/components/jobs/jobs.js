import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './Jobs.module.scss';
import Button from '../../UI/Button/Button';
import { useNavigate } from 'react-router-dom';

function Jobs() {
  let [WorksitesList, setWorksitesList] = useState({});
  let [EmployeesList, setEmployeesList] = useState({});
  let [EquipmentsList, setEquipmentsList] = useState({});

  let [Jobs, setJobs] = useState([]);
  let navigate = useNavigate();

  const getJobs = async function () {

      
      const WorksitesRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites.json');
      let WorksitesData = []
      if (WorksitesRes.data !== null) {
          for (const [key, value] of Object.entries(WorksitesRes.data)) {
              let Worksite = value;
              Worksite.id = key;
              WorksitesData.push(Worksite)
          }                        
      }
      setWorksitesList(WorksitesData);

      const EmployeesRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/employees.json');
      let EmployeesData = []
      if (EmployeesRes.data !== null) {
          for (const [key, value] of Object.entries(EmployeesRes.data)) {
              let Employee = value;
              Employee.id = key;
              EmployeesData.push(Employee)
          }                        
      }
      setEmployeesList(EmployeesData);

      const EquipmentsRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/equipments.json');
      let EquipmentsData = []
      if (EquipmentsRes.data !== null) {
          for (const [key, value] of Object.entries(EquipmentsRes.data)) {
              let Equipment = value;
              Equipment.id = key;
              EquipmentsData.push(Equipment)
          }                        
      }
      setEquipmentsList(EquipmentsData);

      const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs.json');
      let JobsData = []
      if (res.data !== null) {
          for (const [key, value] of Object.entries(res.data)) {
              let Job = value;
              Job.id = key;
              JobsData.push(Job)
          }            
      }
      setJobs(JobsData);
  }

  const deleteJob = async (id) => {
      await axios.delete(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs/${id}.json`);
      getJobs();
  }

  const editJob = (id) => {
      navigate(`/Jobs/edit/${id}`)
  }


  const CreateNewJob = () => {
      navigate('/Jobs/create')
  }

  useEffect(() => {
      getJobs();
  }, []);

  return (
      <div>
          <h2>Jobs List</h2>

          <div className={styles.Table}>
              <div className={styles.Row}>
                  <div className={styles.Cell}>Worksite</div>
                  <div className={styles.Cell}>Type</div>
                  <div className={styles.Cell}>Hazardous materials </div>
                  <div className={styles.Cell}>Employee</div>
                  <div className={styles.Cell}>Service fee</div>
                  <div className={styles.Cell}>Additional equipment</div>
                  <div className={styles.Cell}>Start date</div>
                  <div className={styles.Cell}>End date</div>
                  <div className={styles.Cell}></div>
              </div>
              {Jobs.map((Job, index) => {
                  
                  const Worksite = WorksitesList.find(el => el.id === Job.Worksite);
                  const WorksiteName = Worksite?.name || '';

                  const Employees = EmployeesList.find(el => el.id === Job.Employee);
                  const EmployeesName = Employees?.name || '';

                  const Equipments = EquipmentsList.find(el => el.id === Job.Equipment);
                  const EquipmentName = Equipments?.name || 'None';                 
                  
                  return (
                  <div className={styles.Row} key={index}>
                      <div className={styles.Cell}>{WorksiteName}</div>
                      <div className={styles.Cell}>{Job.type}</div>
                      <div className={styles.Cell}>{Job.HazardousMaterials}</div>
                      <div className={styles.Cell}>{EmployeesName}</div>
                      <div className={styles.Cell}>{Job.ServiceFee}</div>
                      <div className={styles.Cell}>{EquipmentName}</div>
                      <div className={styles.Cell}>{Job.StartDate}</div>
                      <div className={styles.Cell}>{Job.EndDate}</div>

                      <div className={styles.Cell}>
                          <Button
                              type='primary'
                              onClick={() => { editJob(Job.id) }}
                          >
                              Edit
                          </Button>
                          <Button
                              type='error'
                              onClick={() => { deleteJob(Job.id) }}
                          >
                              Delete
                          </Button>
                      </div>
                  </div>
              )})}
          </div>

          <Button
              type='primary'
              onClick={CreateNewJob}
          >
              Create new Job
          </Button>
      </div>
  )
}

export default Jobs;