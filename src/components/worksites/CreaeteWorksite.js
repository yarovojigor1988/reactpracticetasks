import React, { useState, useEffect } from 'react';
import styles from './CreateWorksite.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';
import Select from '../../UI/Select/Select';

export default function CreaetWorksite() {
    let [type, setType] = useState("Company");
    let [name, setName] = useState("");
    let [worksiteAdress, setAdress] = useState("");
    let [status, setStatus] = useState("True");
    let [client, setClient] = useState('');
    let [clients, setClients] = useState([]);


    const setSelect = (e) => {
        setClient(e.target.value);
    }


    const getClients = async function () {
        const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client.json');
        let usersData = []
        if (res.data !== null) {
            for (const [key, value] of Object.entries(res.data)) {
                let user = value;
                user.id = key;
                usersData.push(user)
            }            
        }
        setClients(usersData);
        if (usersData.length > 0 ){
            setClient(usersData[0].id)
        }
    }

    const sendWorksite = async event => {
        event.preventDefault();

        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (worksiteAdress.trim() === '' || worksiteAdress.trim().length < 2 ) {
            alert('Worksite Adress must be longer than 1 character');
            return
        } 

            const worksite = {
            name,
            worksiteAdress,
            type,
            status,
            client,
        }
        try {
            await axios.post('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites.json',worksite);
            setType('Office');
            setName('');
            setAdress('');
            setStatus('True');

        } catch(e) {
            console.log(e);
        }

    }

    useEffect(() => {
        getClients();
    }, []);

    return (
        <div>
            <h2>Create Worksite</h2>

            <form className={styles.WorksitesForm}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                    WorksiteAdress address:
                    <input type="text" name="worksiteAdress" value={worksiteAdress} onChange={e => setAdress(e.target.value)} />
                </label>
                <label>
                    Type:
                    <select value={type} onChange={e => setType(e.target.value)}>
                        <option value="Office">Office</option>
                        <option value="Residential building<">Residential building</option>
                        <option value="individual Apartment">Individual apartment</option>
                        <option value="Manufacturing">Manufacturing</option>
                        <option value="Warehouse">Warehouse</option>
                        <option value="Outdoor">Outdoor</option>
                        <option value="Field">field</option>
                        <option value="other">Other</option>
                    </select>
                </label>

                <Select
                    defaultValue={client}
                    onChange={(e)=>{setSelect(e)}}
                    optionsList={clients}
                    >                    
                    Client
                </Select>

                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='true'>True</option>
                        <option value='false'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={sendWorksite}
                >
                    Send worksite data
                </Button>
            </form>
        </div>
    );
}