import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import styles from './CreateWorksite.module.scss';
import axios from 'axios';
import Button from '../../UI/Button/Button';
import Select from '../../UI/Select/Select';

export default function EditWorksite() {
    let [type, setType] = useState("Company");
    let [name, setName] = useState("");
    let [worksiteAdress, setAdress] = useState("");
    let [status, setStatus] = useState("True");
    let [client, setClient] = useState('');
    let [clients, setClients] = useState([]);

    let { id } = useParams();

    const getClients = async function () {
        const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client.json');
        let usersData = []
        if (res.data !== null) {
            for (const [key, value] of Object.entries(res.data)) {
                let user = value;
                user.id = key;
                usersData.push(user)
            }            
        }
        setClients(usersData);
    }

    useEffect(() => {       
        
        async function getClientsData() {
            getClients();
        }
        
        getClientsData();

        axios.get(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites/${id}.json`).then(res => {
            setType(res.data.type);
            setName(res.data.name);
            setAdress(res.data.worksiteAdress);
            setStatus(res.data.status);
            setClient(res.data.client)
        })
    },[id]);



    let navigate = useNavigate();

    const setSelect = (e) => {
        setClient(e.target.value);
    }


    const sendWorksite = async event => {
        event.preventDefault();

        if (name.trim() === '' || name.trim().length < 2 ) {
            alert('Name must be longer than 1 character');
            return
        } else if (worksiteAdress.trim() === '' || worksiteAdress.trim().length < 2 ) {
            alert('Worksite Adress must be longer than 1 character');
            return
        } 

        const user = {
            name,
            worksiteAdress,
            type,
            status,
            client,
        }
        try {
            await axios.put(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites/${id}.json`,user);
            navigate('/worksites/list')
        } catch(e) {
            console.log(e);
        }
    }

    return (
        <div>
            <h2>Edit Worksite</h2>

            <form className={styles.WorksitesForm}>
                <label>
                    Name:
                    <input type="text" name="name" value={name} onChange={e => setName(e.target.value)} />
                </label>
                <label>
                    WorksiteAdress address:
                    <input type="text" name="worksiteAdress" value={worksiteAdress} onChange={e => setAdress(e.target.value)} />
                </label>
                <label>
                    Type:
                    <select value={type} onChange={e => setType(e.target.value)}>
                        <option value="Office">Office</option>
                        <option value="Residential building">Residential building</option>
                        <option value="individual Apartment">Individual apartment</option>
                        <option value="Manufacturing">Manufacturing</option>
                        <option value="Warehouse">Warehouse</option>
                        <option value="Outdoor">Outdoor</option>
                        <option value="Field">field</option>
                        <option value="other">Other</option>
                    </select>
                </label>

                <Select
                    defaultValue={client}
                    onChange={(e)=>{setSelect(e)}}
                    optionsList={clients}
                    >                    
                    Client
                </Select>

                <label>
                    Status:
                    <select value={status} onChange={e => setStatus(e.target.value)}>
                        <option value='True'>True</option>
                        <option value='False'>False</option>
                    </select>
                </label>

                <Button
                    type="success"
                    onClick={sendWorksite}
                >
                    Send worksite data
                </Button>
            </form>
        </div>
    );
}