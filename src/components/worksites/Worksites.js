import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './Worksites.module.scss';
import Button from '../../UI/Button/Button';
import { useNavigate } from 'react-router-dom';

export default function Worksites() {
    let [clientsList, setClientsList] = useState({});
    let [Worksites, setWorksites] = useState([]);
    let navigate = useNavigate();
    let [jobs, setJobs] = useState([]);

    const getJobs = async function () {
        const jobsRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/jobs.json');
        let jobsData = []
        if (jobsRes.data !== null) {
            for (const [key, value] of Object.entries(jobsRes.data)) {
                let job = value;
                job.id = key;
                jobsData.push(job)
            }            
        }
        setJobs(jobsData);
    }


    const getWorksites = async function () {

        
        const clientsRes = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/client.json');
        let clientsData = []
        if (clientsRes.data !== null) {
            for (const [key, value] of Object.entries(clientsRes.data)) {
                let client = value;
                client.id = key;
                clientsData.push(client)
            }                        
        }
        setClientsList(clientsData);
        const res = await axios.get('https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites.json');
        let WorksitesData = []
        if (res.data !== null) {
            for (const [key, value] of Object.entries(res.data)) {
                let worksite = value;
                worksite.id = key;
                WorksitesData.push(worksite)
            }            
        }
        setWorksites(WorksitesData);
    }

    const deleteWorksite = async (id) => {
        let optionWorksite = jobs.find(el => el.Worksite === id);
        if (typeof(optionWorksite) !== 'undefined') {
            alert('Worksite have active job');
            getWorksites();
            return;
        }

        await axios.delete(`https://rect-practice-default-rtdb.europe-west1.firebasedatabase.app/worksites/${id}.json`);
        getWorksites();
    }

    const editWorksite = (id) => {
        navigate(`/Worksites/edit/${id}`)
    }


    const CreateNewWorksite = () => {
        navigate('/Worksites/create')
    }

    useEffect(() => {
        getWorksites();
        getJobs();
    }, []);

    return (
        <div>
            <h2>Worksites List</h2>

            <div className={styles.WorksitesTable}>
                <div className={styles.WorksitesRow}>
                    <div className={styles.WorksitesCell}>Name</div>
                    <div className={styles.WorksitesCell}>Adress</div>
                    <div className={styles.WorksitesCell}>Type</div>
                    <div className={styles.WorksitesCell}>Status</div>
                    <div className={styles.WorksitesCell}>Client</div>
                    <div className={styles.WorksitesCell}></div>
                </div>
                {Worksites.map((worksite, index) => {
                    const client = clientsList.find(el => el.id === worksite.client);
                    const clientName = client?.name || '';
                    return (
                    <div className={styles.WorksitesRow} key={index}>
                        <div className={styles.WorksitesCell}>{worksite.name}</div>
                        <div className={styles.WorksitesCell}>{worksite.worksiteAdress}</div>
                        <div className={styles.WorksitesCell}>{worksite.type}</div>
                        <div className={styles.WorksitesCell}>{worksite.status}</div>
                        <div className={styles.WorksitesCell}>{clientName}</div>
                        <div className={styles.WorksitesCell}>
                            <Button
                                type="primary"
                                onClick={() => { editWorksite(worksite.id) }}
                            >
                                Edit
                            </Button>
                            <Button
                                type="error"
                                onClick={() => { deleteWorksite(worksite.id) }}
                            >
                                Delete
                            </Button>
                        </div>
                    </div>
                )})}
            </div>

            <Button
                type="primary"
                onClick={CreateNewWorksite}
            >
                Create new Worksite
            </Button>
        </div>
    )
}
